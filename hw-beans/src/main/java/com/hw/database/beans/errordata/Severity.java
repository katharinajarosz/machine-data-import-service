package com.hw.database.beans.errordata;

import javax.annotation.ManagedBean;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The error severity bean.
 * <p>
 * Contain process image identifier.
 * 
 * @author kaja
 * @version 2.0
 *
 */
@Getter @Setter
@Builder @NoArgsConstructor @AllArgsConstructor
@Entity
@Table(name = "Severity", schema = "error")
@ManagedBean
public class Severity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="SeverityId")
	private int id;
	
	@Column(name="Identifier")
	private String identifier;
	
	@Column(name="Severity")
	private String severity;

}
