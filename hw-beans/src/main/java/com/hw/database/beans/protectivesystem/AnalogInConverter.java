package com.hw.database.beans.protectivesystem;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.hw.database.beans.protectivesystem.AnalogIn.AnalogInType;

/**
 * Converter for {@link AnalogIn}.
 * <p>
 * Specifies the conversion of {@link AnalogInType} to {@link String} and vice versa.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Converter
public class AnalogInConverter implements AttributeConverter<AnalogInType, String> {

	@Override
	public String convertToDatabaseColumn(AnalogInType attribute) {
		if (attribute == null) {
			return null;
		}
		return attribute.getType();
	}

	@Override
	public AnalogInType convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return null;
		}
		return AnalogInType.fromType(dbData);
	}
}