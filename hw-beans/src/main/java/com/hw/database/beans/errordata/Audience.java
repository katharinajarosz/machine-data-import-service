package com.hw.database.beans.errordata;

import javax.annotation.ManagedBean;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * An audience bean.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Getter @Setter
@Builder @NoArgsConstructor @AllArgsConstructor 
@Entity
@Table(name = "Audience", schema = "error")
@ManagedBean
public class Audience {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="AudienceId")
	private int id;
	
	private String audience;

}
