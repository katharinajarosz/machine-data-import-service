package com.hw.database.beans.errordata;

import java.io.Serializable;

import javax.annotation.ManagedBean;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The bridge table bean of the relationship between
 * {@link Error}, {@link Description} and {@link Audience}.
 * <p>
 * Contains an embedded id of a composite primary key {@link ErrorDescriptionId}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Getter @Setter
@Builder @NoArgsConstructor @AllArgsConstructor 
@Entity
@Table(name = "ErrorDescription", schema = "error")
@ManagedBean
public class ErrorDescription {
	
	@EmbeddedId
	private ErrorDescriptionId id; 
	
	@ManyToOne
	@JoinColumn(name = "fk_error", insertable = false, updatable = false)
	private Error error;
	
	@ManyToOne
	@JoinColumn(name = "fk_description", insertable = false, updatable = false)
	private Description description;
	
	@ManyToOne
	@JoinColumn(name = "fk_audience", insertable = false, updatable = false)
	private Audience audience;
		
	
	/**
	 * @author kaja
	 *
	 */
	@Embeddable
	@Builder @NoArgsConstructor @AllArgsConstructor
	public static class ErrorDescriptionId implements Serializable {
				
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Column(name = "fk_error")
		private int errorId;
		
		@Column(name = "fk_description")
		private int descriptionId;
		
		@Column(name = "fk_audience")
		private int audienceId;
		
		// FIXME: override equals() and hashCode()
		
	}

}
