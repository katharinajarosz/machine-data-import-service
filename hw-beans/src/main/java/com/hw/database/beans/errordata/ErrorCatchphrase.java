package com.hw.database.beans.errordata;

import java.io.Serializable;

import javax.annotation.ManagedBean;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The bridge table bean of the relationship between
 * {@link Catchphrase} and {@link Error}.
 * <p>
 * Contains an embedded id of a composite primary key {@link ErrorCatchphraseId}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Getter @Setter
@Builder @NoArgsConstructor @AllArgsConstructor 
@Entity
@Table(name = "ErrorCatchphrase", schema = "error")
@ManagedBean
public class ErrorCatchphrase {
	
	@EmbeddedId
	private ErrorCatchphraseId id; 
	
	@ManyToOne
	@JoinColumn(name = "fk_error", insertable = false, updatable = false)
	private Error error;
	
	@ManyToOne
	@JoinColumn(name = "fk_catchphrase", insertable = false, updatable = false)
	private Catchphrase catchphrase;
	
	@Column(name = "weight")
	private double weight;
	
	@Column(name = "comment")
	private String comment;
	
	/**
	 * @author kaja
	 *
	 */
	@Embeddable
	@Builder @NoArgsConstructor @AllArgsConstructor
	public static class ErrorCatchphraseId implements Serializable {
				
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Column(name = "fk_error")
		private int errorId;
		
		@Column(name = "fk_catchphrase")
		private int catchphraseId;
		
		// FIXME: override equals() and hashCode()
		
	}

}
