package com.hw.database.beans.errordata;

import java.io.Serializable;

import javax.annotation.ManagedBean;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The bridge table bean of the relationship between
 * {@link Error}, {@link Solution} and {@link Audience}.
 * <p>
 * Contains an embedded id of a composite primary key {@link ErrorSolutionId}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Getter @Setter
@Builder @NoArgsConstructor @AllArgsConstructor 
@Entity
@Table(name = "ErrorSolution", schema = "error")
@ManagedBean
public class ErrorSolution {
	
	@EmbeddedId
	private ErrorSolutionId id; 
	
	@ManyToOne
	@JoinColumn(name = "fk_error", insertable = false, updatable = false)
	private Error error;
	
	@ManyToOne
	@JoinColumn(name = "fk_solution", insertable = false, updatable = false)
	private Solution solution;
	
	@ManyToOne
	@JoinColumn(name = "fk_audience", insertable = false, updatable = false)
	private Audience audience;
	
	@Column(name = "ASC_ORDER")
	private int order;
	
	
	/**
	 * @author kaja
	 *
	 */
	@Embeddable
	@Builder @NoArgsConstructor @AllArgsConstructor
	public static class ErrorSolutionId implements Serializable {
				
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Column(name = "fk_error")
		private int errorId;
		
		@Column(name = "fk_solution")
		private int solutionId;
		
		@Column(name = "fk_audience")
		private int audienceId;
		
		// FIXME: override equals() and hashCode()
		
	}

}
