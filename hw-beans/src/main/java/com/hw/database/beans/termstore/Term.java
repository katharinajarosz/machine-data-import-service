package com.hw.database.beans.termstore;

import javax.annotation.ManagedBean;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The term bean.
 * <p>
 * Contain unidirectional relationships to {@link TermConstraint}, {@link TermStatus},
 * {@link TermType} and {@link TermUsage}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Getter @Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "Term", schema = "term")
@ManagedBean
public class Term {
	
	@Id
	@Column(name = "TermId", columnDefinition = "HIERARCHYID")
	private Byte[] id;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "TermConstraintId")
	private TermConstraint constraint;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "StatusId")
	private TermStatus status;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "TypeId")
	private TermType type;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "UsageId")
	private TermUsage usage;

}
