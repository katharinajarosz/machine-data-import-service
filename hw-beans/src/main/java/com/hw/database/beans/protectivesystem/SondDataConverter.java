package com.hw.database.beans.protectivesystem;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.hw.database.beans.protectivesystem.SondData.SondDataType;

/**
 * Converter for {@link SondData}.
 * <p>
 * Specifies the conversion of {@link SondDataType} to {@link String} and vice versa.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Converter
public class SondDataConverter implements AttributeConverter<SondDataType, String> {

	@Override
	public String convertToDatabaseColumn(SondDataType attribute) {
		if (attribute == null) {
			return null;
		}
		return attribute.getType();
	}

	@Override
	public SondDataType convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return null;
		}
		return SondDataType.fromType(dbData);
	}
}