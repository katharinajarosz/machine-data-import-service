/**
 * 
 */
package com.hw.database.beans.protectivesystem;

import java.sql.Timestamp;

import javax.annotation.ManagedBean;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.hw.database.beans.globaldata.MachineRun;
import com.hw.database.beans.protectivesystem.SondData.SondDataType;
import com.hw.parser.beans.machinedata.RSondData;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The sond data bean.
 * <p>
 * Contains a bidirectional relationship to {@link MachineRun}.
 * 
 * @author kaja
 * @version 2.0
 * @see RSondData
 */
@Getter @Setter
@Builder @NoArgsConstructor @AllArgsConstructor
@Entity
@Table(name = "SondData", schema = "machine")
@ManagedBean
public class SondData {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "SondDataId")
	private long id;

	@Column(name ="Measured_PH")
	private short upH;
	
	@Column(name ="Measured_Temperature")
	private short uTemp;
	
	@Column(name ="Calculated_Temperature")
	private float temperature;
	
	@Column(name ="Calculated_PH")
	private float ph;
	
	@Column(name = "Machine_Data_Loop_Timestamp")
	private Timestamp machineDataLoopTimestamp;
	
	@Column(name = "Type")
	@Convert(converter = SondDataConverter.class)
	private SondDataType type;
	
	@ManyToOne
	@JoinColumn(name = "Machine_Run_ID")
	private MachineRun machineRun;

	/**
	 * The sond data enum.
	 * <p>
	 * This enum contains the sond names.
	 * 
	 * @author kaja
	 * @version 1.0
	 */
	
	public enum SondDataType {
		SOND_ACID("Sond Acid"), SOND_BASE("Sond Base"), SOND_MONITOR("Sond Monitor"), SOND_RESERVOIR("Sond Reservoir");		

		private final String type;

		SondDataType(String type) {
			this.type = type;
		}

		/**
		 * @return String
		 */
		public String getType() {
			return type;
		}

		/**
		 * @param type
		 * @return SondDataType
		 */
		public static SondDataType fromType(String type) {
			for (SondDataType sondDataType : SondDataType.values()) {
				if (sondDataType.getType().equals(type)) {
					return sondDataType;
				}
			}
			throw new UnsupportedOperationException("The type " + type + " is not supported!");
		}

	}

}
