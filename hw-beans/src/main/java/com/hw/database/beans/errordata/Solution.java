package com.hw.database.beans.errordata;

import javax.annotation.ManagedBean;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.hw.database.beans.globaldata.Internationalization;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The error solution bean.
 * <p>
 * Contains an unidirectional relationship to
 * {@link Internationalization}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Getter @Setter
@Builder @NoArgsConstructor @AllArgsConstructor 
@Entity
@Table(name = "Solution", schema = "error")
@ManagedBean
public class Solution {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="SolutionId")
	private int id;
	
	private String translation;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "IsoId")
	private Internationalization iso;

}
