package com.hw.database.beans.errordata;

import java.util.Set;

import javax.annotation.ManagedBean;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.hw.database.beans.globaldata.Internationalization;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The catchphrase bean.
 * <p>
 * Contains an unidirectional relationship to
 * {@link Internationalization} and a bidirectional relationship
 * to {@link Error}
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Getter @Setter
@Builder @NoArgsConstructor @AllArgsConstructor 
@Entity
@Table(name = "Catchphrase", schema = "error")
@ManagedBean
public class Catchphrase {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="CatchphraseId")
	private int id;
	
	private String translation;
	
	private String description;
	
	@OneToMany(mappedBy = "catchphrase")
	private Set<ErrorCatchphrase> errors;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "IsoId")
	private Internationalization iso;

}
