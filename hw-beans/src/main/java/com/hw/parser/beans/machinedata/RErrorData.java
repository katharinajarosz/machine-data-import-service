/**
 * 
 */
package com.hw.parser.beans.machinedata;

import java.util.List;

import javax.annotation.ManagedBean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The error data process image bean.
 * <p>
 * This bean contains a {@code float} data type and a {@code List} of
 * {@link RErrorElement}.
 * 
 * @author kaja
 * @version 1.0
 */
@Data
@Builder @NoArgsConstructor @AllArgsConstructor
@ManagedBean
public class RErrorData {

	private List<RErrorElement> errorElements;
		
	private float phReservoirRangeCheck;

}
