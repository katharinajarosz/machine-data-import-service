/**
 * 
 */
package com.hw.parser.beans.machinedata;

import javax.annotation.ManagedBean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The system diagnosis data process image bean.
 * <p>
 * This bean contains 16 {@code float} data types.
 * 
 * @author kaja
 * @version 1.0
 */
@Data
@Builder @NoArgsConstructor @AllArgsConstructor
@ManagedBean
public class RSystemDiagnosis {

	
	private float processorLoad;
	
	private float memoryUsage;
	
	private float systemLoopProcessingTime;
	
	private float loopLateCounter;
	
	private float hostTotalProcessorLoad;
	
	private float hostLK2001ProcessorLoad;
	
	private float hostLK2001PrivateBytes;
	
	private float hostReconnectCounter;
	
	private float hostConnectionStageChangeCounter;
	
	private float durationLastSavingProcess;
	
	private float open;
	
	private float setPointerToEnd;
	
	private float sdwrite;
	
	private float sdflush;
	
	private float sdclose;
	
	private float elementInQ;

}
