/**
 * 
 */
package com.hw.parser.beans.machinedata;

import javax.annotation.ManagedBean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The error element process image bean.
 * <p>
 * This bean contains active errors with {@code int} "error id" and {@code int}
 * "error level".
 * 
 * @author kaja
 * @version 1.0
 */
@Data
@Builder @NoArgsConstructor @AllArgsConstructor
@ManagedBean
public class RErrorElement {

	private int errorId;
	
	private int errorLevel;

}
