/**
 * 
 */
package com.hw.parser.beans;

import java.util.List;

import javax.annotation.ManagedBean;

import com.hw.parser.beans.machinedata.RClosedLoop;
import com.hw.parser.beans.machinedata.RDigitalInput;
import com.hw.parser.beans.machinedata.RDigitalOutput;
import com.hw.parser.beans.machinedata.RErrorData;
import com.hw.parser.beans.machinedata.RIOData;
import com.hw.parser.beans.machinedata.RNumericInput;
import com.hw.parser.beans.machinedata.RNumericOutput;
import com.hw.parser.beans.machinedata.RProcessControlData;
import com.hw.parser.beans.machinedata.RProtectiveSystemData;
import com.hw.parser.beans.machinedata.RSystemDiagnosis;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The machine data set process image bean.
 * <p>
 * This bean contains {@link RNumericInput} as {@code List}, {@link RDigitalInput}
 * as {@code List}, {@link RClosedLoop} as {@code List}, {@link RNumericOutput} as
 * {@code List}, {@link RDigitalOutput} as {@code List}, {@link RIOData},
 * {@link RProcessControlData}, {@link RProtectiveSystemData},
 * {@link RSystemDiagnosis} and {@link RErrorData}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Data
@Builder
@NoArgsConstructor @AllArgsConstructor
@ManagedBean
public class RMachineDataSet {

	private List<RNumericInput> numericInput;

	private List<RDigitalInput> digitalInput;

	private List<RClosedLoop> closedLoop;

	private List<RNumericOutput> numericOutput;

	private List<RDigitalOutput> digitalOutput;

	private RIOData ioData;

	private RProcessControlData processControlData;

	private RProtectiveSystemData protectiveSystemData;

	private RSystemDiagnosis systemDiagnosis;

	private RErrorData errorData;

}
