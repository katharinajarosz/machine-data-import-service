/**
 * 
 */
package com.hw.parser.beans.machinedata;

import javax.annotation.ManagedBean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The process control data process image bean.
 * <p>
 * This bean contains 10 {@code byte} data types.
 * 
 * @author kaja
 * @version 1.0
 */
@Data
@Builder @NoArgsConstructor @AllArgsConstructor
@ManagedBean
public class RProcessControlData {

	private byte systemState;
	
	private byte idleState;
	
	private byte startupState;
	
	private byte serviceState;
	
	private byte preparationState;
	
	private byte treatmentState;
	
	private byte postprocessState;
	
	private byte desinfectionState;
	
	private byte activeSubsequence;
	
	private byte subsequenceState;

}
