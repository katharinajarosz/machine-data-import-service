INSERT machine.system_state (system_state_node, system_state_value, system_state_name)  
VALUES (hierarchyid::GetRoot(), 0, 'Main State') ;  
GO

IF OBJECT_ID('AddMainState','P') IS NOT NULL
   DROP PROCEDURE AddMainState
GO

CREATE PROC AddMainState(@stateId int OUTPUT, @stateName varchar(50))   
AS   
BEGIN  
   DECLARE @parentNode hierarchyid, @lastChildNode hierarchyid
   SELECT @parentNode = hierarchyid::GetRoot()
   FROM machine.system_state
   
   SET TRANSACTION ISOLATION LEVEL SERIALIZABLE  
   BEGIN TRANSACTION   
		
      SELECT @lastChildNode = max(system_state_node)
		FROM machine.system_state
		WHERE system_state_node.GetAncestor(1) = @parentNode
	  INSERT machine.system_state (system_state_node, system_state_value, system_state_name)  
      VALUES(@parentNode.GetDescendant(@lastChildNode, NULL), @stateId, @stateName)
	  SET @stateId = @stateId+1;  
   COMMIT  
END ;  
GO

DECLARE @stateId int = 0;

EXEC AddMainState @stateId OUTPUT,'Startup';
EXEC AddMainState @stateId OUTPUT,'Idle';
EXEC AddMainState @stateId OUTPUT,'Service';
EXEC AddMainState @stateId OUTPUT,'Preparation';
EXEC AddMainState @stateId OUTPUT,'Treatment';
EXEC AddMainState @stateId OUTPUT,'Postprocess';
EXEC AddMainState @stateId OUTPUT,'Desinfection';
GO

IF OBJECT_ID('AddMainState','P') IS NOT NULL
   DROP PROCEDURE AddMainState
GO