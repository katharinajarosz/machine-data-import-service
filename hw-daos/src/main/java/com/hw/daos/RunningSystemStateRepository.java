package com.hw.daos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.hw.database.beans.processcontrol.RunningSystemState;

/**
 * The extension of {@link JpaRepository} for {@link RunningSystemState}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Transactional
public interface RunningSystemStateRepository extends JpaRepository<RunningSystemState, Long> {
	
	
//	RunningSystemState findBySystemState(RunningSystemState state);
//	
//	RunningSystemState findBySystemStateAndStart(RunningSystemState state, LocalDateTime start);
//	
//	RunningSystemState findBySystemStateAndStartAndEnd(RunningSystemState state, LocalDateTime start, LocalDateTime end);

}
