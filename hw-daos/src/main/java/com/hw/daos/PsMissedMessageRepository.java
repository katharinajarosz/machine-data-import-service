package com.hw.daos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.hw.database.beans.protectivesystem.PSMissedMessage;

/**
 * The extension of {@link JpaRepository} for {@link PSMissedMessage}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Transactional
public interface PsMissedMessageRepository extends JpaRepository<PSMissedMessage, Long> {
	
	

}
