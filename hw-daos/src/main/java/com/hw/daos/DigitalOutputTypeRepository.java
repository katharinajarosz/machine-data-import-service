package com.hw.daos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.hw.database.beans.controlsystem.types.DigitalOutputType;

/**
 * The extension of {@link JpaRepository} for {@link DigitalOutputType}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Transactional
public interface DigitalOutputTypeRepository extends JpaRepository<DigitalOutputType, Integer> {
	
	/**
	 * @param id
	 * @return a DigitalOutputType object
	 */
	DigitalOutputType findById(int id);

}
