package com.hw.daos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.hw.database.beans.controlsystem.types.HeaterModeType;

/**
 * The extension of {@link JpaRepository} for {@link HeaterModeType}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Transactional
public interface HeaterModeTypeRepository extends JpaRepository<HeaterModeType, Integer> {
	
	HeaterModeType findById(int id);

}
