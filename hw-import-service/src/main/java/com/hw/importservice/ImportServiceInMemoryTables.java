package com.hw.importservice;

import com.hw.daos.*;
import com.hw.database.beans.controlsystem.*;
import com.hw.database.beans.controlsystem.types.*;
import com.hw.database.beans.errordata.ActiveError;
import com.hw.database.beans.errordata.Error;
import com.hw.database.beans.errordata.Severity;
import com.hw.database.beans.globaldata.MachineRun;
import com.hw.database.beans.processcontrol.ProtectState;
import com.hw.database.beans.processcontrol.RunningSystemState;
import com.hw.database.beans.processcontrol.SystemState;
import com.hw.database.beans.processcontrol.SystemStateSubsequence;
import com.hw.database.beans.protectivesystem.AnalogIn.AnalogInType;
import com.hw.database.beans.protectivesystem.PSConfiguration;
import com.hw.database.beans.protectivesystem.PSMissedMessage;
import com.hw.database.beans.protectivesystem.PSStatus;
import com.hw.database.beans.protectivesystem.PumpData.PumpDataType;
import com.hw.database.beans.protectivesystem.SondData.SondDataType;
import com.hw.importservice.converter.MachineDataConverter;
import com.hw.importservice.converter.TemporaryTables;
import com.hw.importservice.converter.TimestampConverter;
import com.hw.importservice.filereader.HwBinFileReader;
import com.hw.parser.beans.RHeaderData;
import com.hw.parser.beans.RMachineDataSet;
import com.hw.parser.beans.machinedata.*;
import com.microsoft.sqlserver.jdbc.SQLServerDataTable;
import com.microsoft.sqlserver.jdbc.SQLServerException;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManagerFactory;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;

@Service
@EnableScheduling
@Slf4j
public class ImportServiceInMemoryTables {

	private static Path hwBin = Paths.get("\\\\", "NAS01", "MachineData", "RohData_LK2001");
	FileTime currentYear = FileTime.from(Instant.parse("2020-01-01T00:00:00.00Z"));	
	private static final int HEADER_SIZE = 1024;
	private static final String FILE_PATTERN = "glob:*.hwbin";
	private static final String FILE_REGEX = ".*([0-9]\\.[0-9]\\.[0-9].*[FDC].*)";
	private static final int[] numericInputIds = { 8, 9, 10, 11, 12, 14, 34, 35, 36, 52, 53, 55, 56, 57, 58, 59 };
	private static final int[] closedLoopIds = { 3, 4, 21, 22, 24, 25, 26, 27 };

	@Getter
	@Setter
	private RMachineDataSet rMachineDataSet;
	@Getter
	@Setter
	private RHeaderData headerData;
	@Getter
	@Setter
	private MachineRun machineRun;
	private TimestampConverter timestampConverter;
	private List<RClosedLoop> closedLoopList = new ArrayList<>();
	private List<RDigitalInput> digitalInputList = new ArrayList<>();
	private List<RDigitalOutput> digitalOutputList = new ArrayList<>();
	private List<RNumericInput> numericInputList = new ArrayList<>();
	private List<RNumericOutput> numericOutputList = new ArrayList<>();
	private SQLServerDataTable tempNumInputs;
	private SQLServerDataTable tempNumOutputs;
	private SQLServerDataTable tempDigInputs;
	private SQLServerDataTable tempDigOutputs;
	private SQLServerDataTable tempClosedloops;
	private SQLServerDataTable tempBalancingData;
	private SQLServerDataTable tempSysDiagnosis;
	private SQLServerDataTable tempAnalogIn;
	private SQLServerDataTable tempPumpData;
	private SQLServerDataTable tempSondData;
	private TemporaryTables temporary;
	private HwBinFileReader hwBinFileReader;
	private ByteBuffer byteBuffer;
	private final PathMatcher matcher;
	private Queue<Path> pathQueue;
	byte[] intByteArr = new byte[4];
	private SystemStateSubsequence[][] systemStateWithSubstate = new SystemStateSubsequence[40][21];
	private SystemState[][] systemState = new SystemState[7][92];
	private List<Integer> numInIds = Arrays.stream(numericInputIds).boxed().collect(Collectors.toList());
	private List<Integer> clLoopIds = Arrays.stream(closedLoopIds).boxed().collect(Collectors.toList());

	@Autowired
	private ErrorRepository errorRepo;
	@Autowired
	private SeverityRepository severityRepo;
	@Autowired
	private MachineDataConverter machineDataConverter;
	@Autowired
	private MachineRunRepository machineRunRepo;
	@Autowired
	private ClosedLoopTypeRepository closedLoopTypeRepo;
	@Autowired
	private DigitalInputTypeRepository digitalInputTypeRepo;
	@Autowired
	private DigitalOutputTypeRepository digitalOutputTypeRepo;
	@Autowired
	private NumericInputTypeRepository numericInputTypeRepo;
	@Autowired
	private NumericOutputTypeRepository numericOutputTypeRepo;
	@Autowired
	private GpModeTypeRepository gpModeTypeRepo;
	@Autowired
	private HeaterModeTypeRepository heaterModeTypeRepo;
	@Autowired
	private PpSupplyModeTypeRepository supplyModeTypeRepo;
	@Autowired
	private PpWasteModeTypeRepository wasteModeTypeRepo;
	@Autowired
	private PsConfigurationRepository psConfigRepo;

	@Autowired
	public ImportServiceInMemoryTables(EntityManagerFactory emf) {
		super();
		this.timestampConverter = new TimestampConverter();
		this.hwBinFileReader = new HwBinFileReader();
		this.pathQueue = new ConcurrentLinkedQueue<>();
		this.temporary = new TemporaryTables();
		Assert.notNull(emf, "EntityManagerFactory must not be null");
		matcher = FileSystems.getDefault().getPathMatcher(FILE_PATTERN);
	}


	@PostConstruct
	private void init() {
		log.info("init matrix started");
		systemState = this.initStateMatrix();
		systemStateWithSubstate = this.initStateMatrixWithSubstate();
		log.info("init matrix completed");
	}


	private SystemState[][] initStateMatrix() {
		for (int i = 0; i <= 6; i++)
			for (int j = 0; j <= 91; j++) {
				Optional<SystemState> state = machineDataConverter.getSystemStateNode(i, j);
				if (state.isPresent()) {
					systemState[i][j] = state.get();
				}
			}
		return systemState;
	}


	private SystemStateSubsequence[][] initStateMatrixWithSubstate() {
		for (int i = 0; i <= 39; i++) {
			for (int j = 0; j <= 20; j++) {
				Optional<SystemStateSubsequence> state = machineDataConverter.getSystemSubsequenceNode(i, j);
				if (state.isPresent()) {
					systemStateWithSubstate[i][j] = state.get();

				}
			}
		}
		return systemStateWithSubstate;
	}


	@Scheduled(fixedRateString = "P1D") // 22,22 h
	public void scanFileSystem() {

		@SuppressWarnings("unused")
		int i = 0;

		try {
			HWBinFileFinder hwBinFileFinder = new HWBinFileFinder();
			Files.walkFileTree(hwBin, hwBinFileFinder);
			while (hwBinFileFinder.getMachineDataSets()) {

			}

		} catch (IOException | InterruptedException e) {

			e.printStackTrace();
		}
	}


	public class HWBinFileFinder extends SimpleFileVisitor<Path> {

		/**
		 * Skip subtrees that contain invalid files.
		 *
		 * @param file  the file
		 * @param attrs the attrs
		 * @return the file visit result
		 */
		@Override
		public FileVisitResult preVisitDirectory(Path file, BasicFileAttributes attrs) {

			String name = file.getFileName().toString();

			switch (name) {

			case "007": 
			case "050": 
			case "051":
			case "055":
			case "852":			
			case "MP1": // doesn't contain LK2001 files
			case "MP2": // doesn't contain LK2001 files	
			case "MP3": // doesn't contain LK2001 files
			case "MP4": // doesn't contain LK2001 files	
				return FileVisitResult.SKIP_SUBTREE;

			}

			return FileVisitResult.CONTINUE;
		}


		@Override
		public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {

			Path name = file.getFileName();
			if (attrs.isRegularFile() && matcher.matches(name)) {

				FileTime created = attrs.creationTime();
				FileTime modified = attrs.lastModifiedTime();
				int c1 = created.compareTo(currentYear);
				int c2 = modified.compareTo(currentYear);
				if (c2 > 0) {
					log.info("File created or modified: " + name);
					
				}
				pathQueue.add(file);
				
			}

			return FileVisitResult.CONTINUE;
		}

		@Override
		public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {

			return super.visitFileFailed(file, exc);
		}


		private void initTempTables() throws SQLServerException {
			tempNumInputs = temporary.addNumericInputMetaData(new SQLServerDataTable());
			tempNumOutputs = temporary.addNumericOutputMetaData(new SQLServerDataTable());
			tempDigInputs = temporary.addDigitalInputMetaData(new SQLServerDataTable());
			tempDigOutputs = temporary.addDigitalOutputMetaData(new SQLServerDataTable());
			tempClosedloops = temporary.addClosedLoopMetaData(new SQLServerDataTable());
			tempBalancingData = temporary.addBalancingMetaData(new SQLServerDataTable());
			tempSysDiagnosis = temporary.addSystemDiagnosisMetaData(new SQLServerDataTable());
			tempAnalogIn = temporary.addAnalogInMetaData(new SQLServerDataTable());
			tempPumpData = temporary.addPumpMetaData(new SQLServerDataTable());
			tempSondData = temporary.addSondMetaData(new SQLServerDataTable());
		}


		private boolean getMachineDataSets() throws InterruptedException {			

			if (pathQueue.peek() != null) {
				Path element = pathQueue.poll();
				try (BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(element.toFile()))) {

					String fileName = element.getFileName().toString();

					if (machineRunRepo.findByFileName(fileName) == null) {
						if (!fileName.matches(FILE_REGEX)) {													

							List<NumericInputType> numericInputTypes = numericInputTypeRepo.findAll();
							List<NumericOutputType> numericOutputTypes = numericOutputTypeRepo.findAll();
							List<DigitalInputType> digitalInputTypes = digitalInputTypeRepo.findAll();
							List<DigitalOutputType> digitalOutputTypes = digitalOutputTypeRepo.findAll();
							List<ClosedLoopType> closedLoopTypes = closedLoopTypeRepo.findAll();
							List<PpSupplyModeType> supplyModeTypes = supplyModeTypeRepo.findAll();
							List<PpWasteModeType> wasteModeTypes = wasteModeTypeRepo.findAll();
							List<GpModeType> gpModeTypes = gpModeTypeRepo.findAll();
							List<HeaterModeType> heaterModeTypes = heaterModeTypeRepo.findAll();
							List<Severity> severities = severityRepo.findAll();
							List<Error> errors = errorRepo.findAll();
							List<ActiveError> completeActiveErrors = new ArrayList<>();
							List<ActiveError> lastActiveErrors = new ArrayList<>();
							List<RErrorElement> currentErrorElements = new ArrayList<>();
							List<IOData> completeIoData = new ArrayList<>();
							IOData lastIoData = new IOData();
							List<PSMissedMessage> completePsMissedMessage = new ArrayList<>();
							PSMissedMessage lastMissedMessage = new PSMissedMessage();
							List<PSStatus> completePsStatus = new ArrayList<>();
							PSStatus lastPsStatus = new PSStatus();
							List<ProtectState> completeProtectStates = new ArrayList<>();
							ProtectState lastProtectStates = new ProtectState();
							List<RunningSystemState> completeSystemStates = new ArrayList<>();
							RunningSystemState lastSystemState = new RunningSystemState();
							Long lastTimestamp = 0l;
							Long lastTimestamp2 = 0l;
							int loop = 0;
							this.initTempTables();							
							boolean balanceOffset = this.matchVersion(fileName);

							machineRun = machineDataConverter.createGlobalDataWithoutHeader(fileName);
							machineRun = machineRunRepo.saveAndFlush(machineRun);														

							if (this.matchFileVersion(fileName)) {

								byte[] buff = new byte[HEADER_SIZE];
								inputStream.read(buff, 0, HEADER_SIZE);																								

							}

							// machine data set loop							
							int readBytes;
							while ((readBytes = inputStream.read(intByteArr)) != -1) {

								byte[] b = new byte[1];																

								int fromByteArray = ByteBuffer.wrap(intByteArr).getInt();																

								if (fromByteArray > 0) {

									byte[] data = new byte[fromByteArray];
									inputStream.read(data);
									byteBuffer = ByteBuffer.wrap(data);
									rMachineDataSet = hwBinFileReader.readMachineDataSet(byteBuffer, balanceOffset);
									byteBuffer.position(fromByteArray);
									byteBuffer.clear();
									inputStream.read(b);

									// create machine run start
									setMachineRunStart();

									

									tempBalancingData = createBalancingData(tempBalancingData);
									tempSysDiagnosis = createSystemDiagnosis(tempSysDiagnosis);
									tempAnalogIn = createAnalogIn(tempAnalogIn);
									tempPumpData = createPumpData(tempPumpData);
									tempSondData = createSondData(tempSondData);

									// create closed loops
									tempClosedloops = handleClosedLoops(closedLoopTypes, tempClosedloops);

									// create numeric inputs
									tempNumInputs = handleNumericInputs(numericInputTypes, tempNumInputs);

									// create digital inputs
									tempDigInputs = handleDigitalInputs(digitalInputTypes, tempDigInputs);

									// create numeric outputs
									tempNumOutputs = handleNumericOutputs(numericOutputTypes, tempNumOutputs);

									// create digital outputs
									tempDigOutputs = handleDigitalOutputs(digitalOutputTypes, tempDigOutputs);

									// create active errors
									currentErrorElements = getActiveErrorsOnly(currentErrorElements);

									// new errors
									lastActiveErrors = addNewErrors(lastActiveErrors, currentErrorElements, severities, errors);

									// canceled errors
									lastActiveErrors = handleCanceledErrors(completeActiveErrors, lastActiveErrors, currentErrorElements, lastTimestamp, lastTimestamp2);

									// create io data
									lastIoData = handleIoData(supplyModeTypes, wasteModeTypes, gpModeTypes,
											heaterModeTypes, completeIoData, lastIoData, lastTimestamp, lastTimestamp2);

									// create ps missed messages
									lastMissedMessage = handleMissedMessages(lastMissedMessage, completePsMissedMessage,
											lastTimestamp, lastTimestamp2);

									// create ps status
									lastPsStatus = handlePsStatus(lastPsStatus, completePsStatus, lastTimestamp,
											lastTimestamp2);

									// create protect state
									lastProtectStates = handleProtectStates(lastProtectStates, completeProtectStates,
											lastTimestamp, lastTimestamp2);

									// create system state
									lastSystemState = handleSystemStates(lastSystemState, completeSystemStates,
											lastTimestamp, lastTimestamp2);

									lastTimestamp = rMachineDataSet.getIoData().getTimestamp1();
									lastTimestamp2 = rMachineDataSet.getIoData().getTimestamp2();

									loop++;

									if (loop % 1000 == 0 && loop > 0) {

										temporary.persistAnalodIn(tempAnalogIn);

										temporary.persistBalancingData(tempBalancingData);

										temporary.persistClosedLoop(tempClosedloops);

										temporary.persistDigitalInput(tempDigInputs);

										temporary.persistDigitalOutput(tempDigOutputs);

										temporary.persistNumericInput(tempNumInputs);

										temporary.persistNumericOutput(tempNumOutputs);

										temporary.persistPumpData(tempPumpData);

										temporary.persistSondData(tempSondData);

										temporary.persistSystemDiagnosis(tempSysDiagnosis);

										this.initTempTables();

									}

								}
							}
							
							// create ps configuration
							setMachineRunPsConfig();

							// convert last timestamp
							Timestamp end = timestampConverter.convert(lastTimestamp, lastTimestamp2);

							// close all remaining errors
							completeActiveErrors = closeAllRemainingErrors(completeActiveErrors, lastActiveErrors, end);

							// set io data
							completeIoData = setIoData(completeIoData, lastIoData, end);

							// set missed messages
							completePsMissedMessage = setMissedMessages(completePsMissedMessage, lastMissedMessage);

							// set ps status
							completePsStatus = setPsStatus(completePsStatus, lastPsStatus);

							// set protect state
							completeProtectStates = setProtectStates(completeProtectStates, lastProtectStates);

							// set system state
							completeSystemStates = setSystemStates(completeSystemStates, lastSystemState);

							// create machine run end and set lists
							machineRun.setActiveErrors(completeActiveErrors);							
							machineRun.setProtectStates(completeProtectStates);
							machineRun.setPsStatus(completePsStatus);
							machineRun.setPsMissedMessage(completePsMissedMessage);
							machineRun.setIoData(completeIoData);
							
							machineRun.setSystemStates(completeSystemStates);
							machineRun.setEnd(end);													

							machineRunRepo.save(machineRun);														

							temporary.persistAnalodIn(tempAnalogIn);
							temporary.persistBalancingData(tempBalancingData);
							temporary.persistClosedLoop(tempClosedloops);
							temporary.persistDigitalInput(tempDigInputs);
							temporary.persistDigitalOutput(tempDigOutputs);
							temporary.persistNumericInput(tempNumInputs);
							temporary.persistNumericOutput(tempNumOutputs);
							temporary.persistPumpData(tempPumpData);
							temporary.persistSondData(tempSondData);
							temporary.persistSystemDiagnosis(tempSysDiagnosis);

							log.info("Machine ID: " + machineRun.getID() + " saved!");

							machineRun = null;
						}
					}
				} catch (IOException | SQLException e) {

					e.printStackTrace();
				}
				
				return true;
			}

			return false;
		}


		private SQLServerDataTable createSystemDiagnosis(SQLServerDataTable ssdt) throws SQLServerException {

			ssdt.addRow(
					timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
							rMachineDataSet.getIoData().getTimestamp2()),
					rMachineDataSet.getSystemDiagnosis().getHostLK2001PrivateBytes(),
					rMachineDataSet.getSystemDiagnosis().getHostLK2001ProcessorLoad(),
					rMachineDataSet.getSystemDiagnosis().getHostReconnectCounter(),
					rMachineDataSet.getSystemDiagnosis().getHostTotalProcessorLoad(),
					rMachineDataSet.getSystemDiagnosis().getLoopLateCounter(),
					rMachineDataSet.getSystemDiagnosis().getMemoryUsage(),
					rMachineDataSet.getSystemDiagnosis().getProcessorLoad(),
					rMachineDataSet.getSystemDiagnosis().getSystemLoopProcessingTime(), machineRun.getID());
			return ssdt;

		}


		private SQLServerDataTable createBalancingData(SQLServerDataTable ssdt) throws SQLServerException {

			ssdt.addRow(
					timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
							rMachineDataSet.getIoData().getTimestamp2()),
					rMachineDataSet.getProtectiveSystemData().getBalancingData().getAcidFlowConrol(),
					rMachineDataSet.getProtectiveSystemData().getBalancingData().getAcidWeight(),
					rMachineDataSet.getProtectiveSystemData().getBalancingData().getBaseFlowControl(),
					rMachineDataSet.getProtectiveSystemData().getBalancingData().getBaseWeight(),
					rMachineDataSet.getProtectiveSystemData().getBalancingData().getScale1row(),
					rMachineDataSet.getProtectiveSystemData().getBalancingData().getScale2row(),
					rMachineDataSet.getProtectiveSystemData().getBalancingData().getScaleDeviation(),
					rMachineDataSet.getProtectiveSystemData().getBalancingData().getScaleSumCurrent(),
					rMachineDataSet.getProtectiveSystemData().getBalancingData().getUfControl(),
					rMachineDataSet.getProtectiveSystemData().getBalancingData().getUfHost(),
					rMachineDataSet.getProtectiveSystemData().getBalancingData().getBalanceOffset(),
					rMachineDataSet.getProtectiveSystemData().getBalancingData().getUfWeight(), machineRun.getID());
			return ssdt;

		}


		private SQLServerDataTable createAnalogIn(SQLServerDataTable ssdt) throws SQLServerException {

			for (int a = 0; a < 10; a++) {
				ssdt.addRow(
						timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
								rMachineDataSet.getIoData().getTimestamp2()),
						AnalogInType.values()[a].getType(),
						rMachineDataSet.getProtectiveSystemData().getAnalogIn().get(a).getCd(),
						rMachineDataSet.getProtectiveSystemData().getAnalogIn().get(a).getF(), machineRun.getID());
			}
			return ssdt;
		}


		private SQLServerDataTable createPumpData(SQLServerDataTable ssdt) throws SQLServerException {

			for (int a = 0; a < 2; a++) {
				ssdt.addRow(
						timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
								rMachineDataSet.getIoData().getTimestamp2()),
						PumpDataType.values()[a].getType(),
						rMachineDataSet.getProtectiveSystemData().getPumpData().get(a).getControl(),
						rMachineDataSet.getProtectiveSystemData().getPumpData().get(a).getHost(),
						rMachineDataSet.getProtectiveSystemData().getPumpData().get(a).getMeas(), machineRun.getID());

			}
			return ssdt;
		}


		private SQLServerDataTable createSondData(SQLServerDataTable ssdt) throws SQLServerException {

			for (int a = 0; a < 4; a++) {
				ssdt.addRow(
						timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
								rMachineDataSet.getIoData().getTimestamp2()),
						SondDataType.values()[a].getType(),
						rMachineDataSet.getProtectiveSystemData().getSondData().get(a).getPh(),
						rMachineDataSet.getProtectiveSystemData().getSondData().get(a).getTemperature(),
						rMachineDataSet.getProtectiveSystemData().getSondData().get(a).getUTemp(),
						rMachineDataSet.getProtectiveSystemData().getSondData().get(a).getUpH(), machineRun.getID());

			}
			return ssdt;
		}


		private IOData handleIoData(List<PpSupplyModeType> supplyModeTypes, List<PpWasteModeType> wasteModeTypes,
				List<GpModeType> gpModeTypes, List<HeaterModeType> heaterModeTypes, List<IOData> completeIOData,
				IOData lastIoData, Long lastTimestamp, Long lastTimestamp2) {

			int smtId = rMachineDataSet.getIoData().getSupplyMode();
			PpSupplyModeType smt = findSupplyModeTypeIfExist(supplyModeTypes, smtId);

			if (smt == null) {
				smt = supplyModeTypeRepo.saveAndFlush(PpSupplyModeType.builder().id(smtId).build());
				supplyModeTypes.add(smt);
			}

			int wmtId = rMachineDataSet.getIoData().getWasteMode();
			PpWasteModeType wmt = findWasteModeTypeIfExist(wasteModeTypes, wmtId);

			if (wmt == null) {
				wmt = wasteModeTypeRepo.saveAndFlush(PpWasteModeType.builder().id(wmtId).build());
				wasteModeTypes.add(wmt);
			}

			int gmtId = rMachineDataSet.getIoData().getGpMode();
			GpModeType gmt = findGpModeTypeIfExist(gpModeTypes, gmtId);

			if (gmt == null) {
				gmt = gpModeTypeRepo.saveAndFlush(GpModeType.builder().id(gmtId).build());
				gpModeTypes.add(gmt);
			}

			int hmtId = rMachineDataSet.getIoData().getHeaterMode();
			HeaterModeType hmt = findHeaterModeTypeIfExist(heaterModeTypes, hmtId);

			if (hmt == null) {
				hmt = heaterModeTypeRepo.saveAndFlush(HeaterModeType.builder().id(hmtId).build());
				heaterModeTypes.add(hmt);
			}

			IOData ioData = IOData.builder().supplyMode(smt).wasteMode(wmt).gpMode(gmt).heaterMode(hmt).build();

			if (lastIoData.getGpMode() == null && lastIoData.getHeaterMode() == null
					&& lastIoData.getSupplyMode() == null && lastIoData.getWasteMode() == null) {
				ioData.setStart(timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
						rMachineDataSet.getIoData().getTimestamp2()));
				lastIoData = ioData;
			}

			if (!lastIoData.getSupplyMode().equals(smt) && !lastIoData.getWasteMode().equals(wmt)
					&& !lastIoData.getGpMode().equals(gmt) && !lastIoData.getHeaterMode().equals(hmt)) {
				lastIoData.setEnd(timestampConverter.convert(lastTimestamp, lastTimestamp2));
				lastIoData.setMachineRun(machineRun);
				completeIOData.add(lastIoData);
				ioData.setStart(timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
						rMachineDataSet.getIoData().getTimestamp2()));
				lastIoData = ioData;
			}

			return lastIoData;
		}


		private List<ActiveError> addNewErrors(List<ActiveError> lastActiveErrors,
				List<RErrorElement> currentErrorElements, List<Severity> severities, List<Error> errors) {

			List<Integer> lastIds = new ArrayList<>();
			for (ActiveError activeError : lastActiveErrors) {
				lastIds.add(activeError.getError().getId());
			}

			for (RErrorElement rErrorElement : currentErrorElements) {

				if (!lastIds.contains(rErrorElement.getErrorId())) {
					int errorId = rErrorElement.getErrorId();
					Error error = findErrorIfExist(errors, errorId);

					if (error == null) {
						error = errorRepo.saveAndFlush(Error.builder().Id(errorId).build());
						errors.add(error);
					}

					int severityId = rErrorElement.getErrorLevel();
					Severity severity = findErrorLevelIfExist(severities, severityId);

					if (severity == null) {
						severity = severityRepo
								.saveAndFlush(Severity.builder().id(severityId).build());
						severities.add(severity);
					}

					ActiveError errorX = ActiveError.builder().error(error).severity(severity)
							.start(timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
									rMachineDataSet.getIoData().getTimestamp2()))
							.build();
					lastActiveErrors.add(errorX);
				}
			}

			return lastActiveErrors;
		}


		private List<ActiveError> handleCanceledErrors(List<ActiveError> completeActiveErrors,
				List<ActiveError> lastActiveErrors, List<RErrorElement> currentErrorElements, Long lastTimestamp,
				Long lastTimestamp2) {

			List<Integer> currentIds = new ArrayList<>();
			for (RErrorElement currentError : currentErrorElements) {
				currentIds.add(currentError.getErrorId());
			}

			List<ActiveError> deltaActiveErrors = new ArrayList<>();
			deltaActiveErrors.addAll(lastActiveErrors);

			for (ActiveError activeError : deltaActiveErrors) {
				if (!currentIds.contains(activeError.getError().getId())) {
					lastActiveErrors.remove(activeError);
					activeError.setEnd(timestampConverter.convert(lastTimestamp, lastTimestamp2));
					activeError.setMachineRun(machineRun);
					completeActiveErrors.add(activeError);
				}
			}

			return lastActiveErrors;
		}


		private List<RunningSystemState> setSystemStates(List<RunningSystemState> completeSystemStates,
				RunningSystemState lastSystemState) {

			lastSystemState.setEnd(timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
					rMachineDataSet.getIoData().getTimestamp2()));
			lastSystemState.setMachineRun(machineRun);
			completeSystemStates.add(lastSystemState);

			return completeSystemStates;
		}


		private List<ProtectState> setProtectStates(List<ProtectState> completeProtectStates,
				ProtectState lastProtectStates) {

			lastProtectStates.setEnd(timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
					rMachineDataSet.getIoData().getTimestamp2()));
			lastProtectStates.setMachineRun(machineRun);
			completeProtectStates.add(lastProtectStates);

			return completeProtectStates;
		}


		private List<PSStatus> setPsStatus(List<PSStatus> completePsStatus, PSStatus lastPsStatus) {

			lastPsStatus.setEnd(timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
					rMachineDataSet.getIoData().getTimestamp2()));
			lastPsStatus.setMachineRun(machineRun);
			completePsStatus.add(lastPsStatus);

			return completePsStatus;
		}


		private List<PSMissedMessage> setMissedMessages(List<PSMissedMessage> completePsMissedMessage,
				PSMissedMessage lastPsMissedMessage) {

			lastPsMissedMessage.setEnd(timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
					rMachineDataSet.getIoData().getTimestamp2()));
			lastPsMissedMessage.setMachineRun(machineRun);
			completePsMissedMessage.add(lastPsMissedMessage);

			return completePsMissedMessage;
		}


		private List<IOData> setIoData(List<IOData> completeIoData, IOData lastIoData, Timestamp end) {

			lastIoData.setEnd(end);
			lastIoData.setMachineRun(machineRun);
			completeIoData.add(lastIoData);
			return completeIoData;
		}


		private List<ActiveError> closeAllRemainingErrors(List<ActiveError> completeActiveErrors,
				List<ActiveError> lastActiveErrors, Timestamp end) {

			for (ActiveError activeError : lastActiveErrors) {
				activeError.setEnd(end);
				activeError.setMachineRun(machineRun);
				completeActiveErrors.add(activeError);
			}
			return completeActiveErrors;
		}


		private List<RErrorElement> getActiveErrorsOnly(List<RErrorElement> currentErrorElements) {
			List<RErrorElement> errorElements;
			errorElements = rMachineDataSet.getErrorData().getErrorElements();
			int size = errorElements.size();
			// get only active errors
			for (int i = 0; i < size; i++) {
				if (errorElements.get(i).getErrorId() == 0) {
					currentErrorElements = errorElements.subList(0, i == 0 ? 0 : i);
					break;
				}
			}
			return currentErrorElements;
		}

		private RunningSystemState handleSystemStates(RunningSystemState lastSystemState,
				List<RunningSystemState> completeSystemStates, Long lastTimestamp, Long lastTimestamp2) {

			int mainState = rMachineDataSet.getProcessControlData().getSystemState();
			int stateValue = this.getStateValue(mainState);
			int activeSubState = rMachineDataSet.getProcessControlData().getActiveSubsequence();
			int subState = rMachineDataSet.getProcessControlData().getSubsequenceState();

			RunningSystemState state = RunningSystemState.builder().systemStateNode(systemState[mainState][stateValue])
					.build();

			String systemStateNode = state.getSystemStateNode().getNodeString();

			if (activeSubState > 0) {
				state.setSubsequenceNode(systemStateWithSubstate[activeSubState][subState]);
				systemStateNode = systemStateNode.concat(state.getSubsequenceNode().getNodeString());
			}

			if (lastSystemState.getSystemStateNode() == null) {
				state.setStart(timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
						rMachineDataSet.getIoData().getTimestamp2()));
				lastSystemState = state;
			}

			String lastNodeString = lastSystemState.getSystemStateNode().getNodeString();

			if (lastSystemState.getSubsequenceNode() != null) {
				String lastSequenceString = lastSystemState.getSubsequenceNode().getNodeString();
				lastNodeString = lastNodeString.concat(lastSequenceString);
			}

			if (!lastNodeString.equalsIgnoreCase(systemStateNode)) {
				lastSystemState.setEnd(timestampConverter.convert(lastTimestamp, lastTimestamp2));
				lastSystemState.setMachineRun(machineRun);
				completeSystemStates.add(lastSystemState);
				state.setStart(timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
						rMachineDataSet.getIoData().getTimestamp2()));
				lastSystemState = state;
			}

			return lastSystemState;
		}


		private ProtectState handleProtectStates(ProtectState lastProtectStates,
				List<ProtectState> completeProtectStates, Long lastTimestamp, Long lastTimestamp2) {
			ProtectState protectState = ProtectState.builder()
					.protectState(rMachineDataSet.getProtectiveSystemData().getProtectState())
					.protectSubstate(rMachineDataSet.getProtectiveSystemData().getProtectSubstate()).build();

			if (lastProtectStates.getStart() == null) {
				protectState.setStart(timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
						rMachineDataSet.getIoData().getTimestamp2()));
				lastProtectStates = protectState;
			}

			if (lastProtectStates.getProtectState() != protectState.getProtectState()
					&& lastProtectStates.getProtectSubstate() != protectState.getProtectSubstate()) {
				lastProtectStates.setEnd(timestampConverter.convert(lastTimestamp, lastTimestamp2));
				lastProtectStates.setMachineRun(machineRun);
				completeProtectStates.add(lastProtectStates);
				protectState.setStart(timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
						rMachineDataSet.getIoData().getTimestamp2()));
				lastProtectStates = protectState;
			}

			return lastProtectStates;
		}


		private PSStatus handlePsStatus(PSStatus lastPsStatus, List<PSStatus> completePsStatus, Long lastTimestamp,
				Long lastTimestamp2) {
			PSStatus psStatus = PSStatus.builder()
					.controlStatusByte(rMachineDataSet.getProtectiveSystemData().getControlStatusByte())
					.protectStatusByte(rMachineDataSet.getProtectiveSystemData().getProtectStatusByte()).build();

			if (lastPsStatus.getStart() == null) {
				psStatus.setStart(timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
						rMachineDataSet.getIoData().getTimestamp2()));
				lastPsStatus = psStatus;
			}

			if (lastPsStatus.getControlStatusByte() != psStatus.getControlStatusByte()
					&& lastPsStatus.getProtectStatusByte() != psStatus.getProtectStatusByte()) {
				lastPsStatus.setEnd(timestampConverter.convert(lastTimestamp, lastTimestamp2));
				lastPsStatus.setMachineRun(machineRun);
				completePsStatus.add(lastPsStatus);
				psStatus.setStart(timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
						rMachineDataSet.getIoData().getTimestamp2()));
				lastPsStatus = psStatus;
			}
			return lastPsStatus;
		}


		private PSMissedMessage handleMissedMessages(PSMissedMessage lastMissedMessage,
				List<PSMissedMessage> allMissedMessages, Long lastTimestamp, Long lastTimestamp2) {
			PSMissedMessage missedMessage = PSMissedMessage.builder()
					.messageCounterError(rMachineDataSet.getProtectiveSystemData().getMessageCounterErrorCount())
					.sendMessageCounter(rMachineDataSet.getProtectiveSystemData().getSendMsgCounter()).build();

			if (lastMissedMessage.getStart() == null) {
				missedMessage.setStart(timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
						rMachineDataSet.getIoData().getTimestamp2()));
				lastMissedMessage = missedMessage;
			}

			if (lastMissedMessage.getMessageCounterError() != missedMessage.getMessageCounterError()
					&& lastMissedMessage.getSendMessageCounter() != missedMessage.getSendMessageCounter()) {
				lastMissedMessage.setEnd(timestampConverter.convert(lastTimestamp, lastTimestamp2));
				lastMissedMessage.setMachineRun(machineRun);
				allMissedMessages.add(lastMissedMessage);
				missedMessage.setStart(timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
						rMachineDataSet.getIoData().getTimestamp2()));
				lastMissedMessage = missedMessage;
			}

			return lastMissedMessage;
		}


		private HeaterModeType findHeaterModeTypeIfExist(List<HeaterModeType> heaterModeTypes, int hmtId) {
			return heaterModeTypes.stream().filter(input -> input.getId() == hmtId).findFirst().orElse(null);
		}


		private GpModeType findGpModeTypeIfExist(List<GpModeType> gpModeTypes, int gmtId) {
			return gpModeTypes.stream().filter(input -> input.getId() == gmtId).findFirst().orElse(null);
		}


		private PpWasteModeType findWasteModeTypeIfExist(List<PpWasteModeType> wasteModeTypes, int wmtId) {
			return wasteModeTypes.stream().filter(input -> input.getId() == wmtId).findFirst().orElse(null);
		}


		private PpSupplyModeType findSupplyModeTypeIfExist(List<PpSupplyModeType> supplyModeTypes, int smtId) {
			return supplyModeTypes.stream().filter(input -> input.getId() == smtId).findFirst().orElse(null);
		}


		private Severity findErrorLevelIfExist(List<Severity> severities, int severityId) {
			return severities.stream().filter(input -> input.getId() == severityId).findFirst().orElse(null);
		}


		private Error findErrorIfExist(List<Error> errors, int errorId) {
			return errors.stream().filter(input -> input.getId() == errorId).findFirst().orElse(null);
		}


		private SQLServerDataTable handleDigitalOutputs(List<DigitalOutputType> digitalOutputTypes,
				SQLServerDataTable tempDigOutputs) {
			List<DigitalOutput> digitalOutputs = new ArrayList<>();
			digitalOutputList = rMachineDataSet.getDigitalOutput();
			for (RDigitalOutput digitalOutput : digitalOutputList) {

				int piId = digitalOutput.getDigitalOutputs();
				DigitalOutputType type = digitalOutputTypes.stream().filter(input -> input.getId() == piId).findFirst().orElse(null);

				if (type == null) {
					type = digitalOutputTypeRepo.saveAndFlush(DigitalOutputType.builder().id(piId).build());
					digitalOutputTypes.add(type);
				}

				boolean hmi = digitalOutput.isHmi();
				boolean dem = digitalOutput.isDem();

				DigitalOutput digitalOutX = DigitalOutput.builder().machineRun(machineRun)
						.machineDataLoopTimestamp(
								timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
										rMachineDataSet.getIoData().getTimestamp2()))
						.type(type).hmi(hmi).dem(dem).build();
				digitalOutputs.add(digitalOutX);
			}

			digitalOutputs.forEach(item -> {
				try {
					tempDigOutputs.addRow(item.getMachineDataLoopTimestamp(), item.isDem(), item.isHmi(),
							item.getMachineRun().getID(), item.getType().getId());
				} catch (SQLServerException e) {
					e.printStackTrace();
				}
			});

			return tempDigOutputs;
		}


		private SQLServerDataTable handleNumericOutputs(List<NumericOutputType> numericOutputTypes,
				SQLServerDataTable tempNumOutputs) {
			List<NumericOutput> numericOutputs = new ArrayList<>();
			numericOutputList = rMachineDataSet.getNumericOutput();
			for (RNumericOutput numericOutput : numericOutputList) {

				int piId = numericOutput.getNumericOutput();
				NumericOutputType type = numericOutputTypes.stream().filter(input -> input.getId() == piId).findFirst().orElse(null);

				if (type == null) {
					type = numericOutputTypeRepo.saveAndFlush(NumericOutputType.builder().id(piId).build());
					numericOutputTypes.add(type);
				}

				float dem = numericOutput.getDem();
				float cd = numericOutput.getCd();

				if (cd != 0 || dem != 0) {
					NumericOutput numericOutX = NumericOutput.builder()
							.machineDataLoopTimestamp(
									timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
											rMachineDataSet.getIoData().getTimestamp2()))
							.machineRun(machineRun).type(type).dem(dem).cd(cd).build();
					numericOutputs.add(numericOutX);
				}
			}

			numericOutputs.forEach(item -> {
				try {
					tempNumOutputs.addRow(item.getMachineDataLoopTimestamp(), item.getCd(), item.getDem(),
							item.getMachineRun().getID(), item.getType().getId());
				} catch (SQLServerException e) {
					e.printStackTrace();
				}
			});

			return tempNumOutputs;
		}


		private SQLServerDataTable handleDigitalInputs(List<DigitalInputType> digitalInputTypes,
				SQLServerDataTable tempDigInputs) {
			List<DigitalInput> digitalInputs = new ArrayList<>();
			digitalInputList = rMachineDataSet.getDigitalInput();
			for (RDigitalInput digitalInput : digitalInputList) {

				int piId = digitalInput.getDigitalInput();
				DigitalInputType type = digitalInputTypes.stream().filter(input -> input.getId() == piId).findFirst().orElse(null);

				if (type == null) {
					type = digitalInputTypeRepo.saveAndFlush(DigitalInputType.builder().id(piId).build());
					digitalInputTypes.add(type);
				}

				boolean cd = digitalInput.isCd();
				boolean r = digitalInput.isR();

				DigitalInput digitalInX = DigitalInput.builder()

						.machineDataLoopTimestamp(
								timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
										rMachineDataSet.getIoData().getTimestamp2()))
						.machineRun(machineRun).type(type).cd(cd).r(r).build();
				digitalInputs.add(digitalInX);

			}

			digitalInputs.forEach(item -> {
				try {
					tempDigInputs.addRow(item.getMachineDataLoopTimestamp(), item.isCd(), item.isR(),
							item.getMachineRun().getID(), item.getType().getId());
				} catch (SQLServerException e) {
					e.printStackTrace();
				}
			});

			return tempDigInputs;
		}


		private SQLServerDataTable handleNumericInputs(List<NumericInputType> numericInputTypes,
				SQLServerDataTable tempNumInputs) {
			List<NumericInput> numericInputs = new ArrayList<>();
			numericInputList = rMachineDataSet.getNumericInput();
			for (RNumericInput numericInput : numericInputList) {

				int piId = numericInput.getNumericInput();
				NumericInputType type = numericInputTypes.stream().filter(input -> input.getId() == piId).findFirst().orElse(null);

				if (type == null) {
					type = numericInputTypeRepo.saveAndFlush(NumericInputType.builder().id(piId).build());
					numericInputTypes.add(type);
				}

				float r = numericInput.getR();
				float f = numericInput.getF();
				float cd = numericInput.getCd();

				if (numInIds.contains(piId)) {
					
					if (r != 0 || f != 0 || cd != 0) {
						NumericInput numericInX = NumericInput.builder()
								.machineDataLoopTimestamp(
										timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
												rMachineDataSet.getIoData().getTimestamp2()))
								.machineRun(machineRun).type(type).r(r).f(f).cd(cd).build();
						numericInputs.add(numericInX);
					}
				}
				

			}

			numericInputs.forEach(item -> {

				try {
					tempNumInputs.addRow(item.getMachineDataLoopTimestamp(), item.getCd(), item.getF(), item.getR(),
							item.getMachineRun().getID(), item.getType().getId());
				} catch (SQLServerException e) {
					e.printStackTrace();
				}
			});

			return tempNumInputs;
		}


		private SQLServerDataTable handleClosedLoops(List<ClosedLoopType> closedLoopTypes,
				SQLServerDataTable tempClosedloops) {
			List<ClosedLoop> closedLoops = new ArrayList<>();
			closedLoopList = rMachineDataSet.getClosedLoop();
			for (RClosedLoop closedLoop : closedLoopList) {

				int piId = closedLoop.getClosedLoop();
				ClosedLoopType type = closedLoopTypes.stream().filter(input -> input.getId() == piId).findFirst().orElse(null);

				if (type == null) {
					type = closedLoopTypeRepo.saveAndFlush(ClosedLoopType.builder().id(piId).build());
					closedLoopTypes.add(type);
				}

				float hmi = closedLoop.getHmi();
				float proc = closedLoop.getProc();
				float dem = closedLoop.getDem();
				float y = closedLoop.getY();
				float inp = closedLoop.getInp();
				float e = closedLoop.getE();

				if (clLoopIds.contains(piId)) {
					if (hmi != 0 || proc != 0 || dem != 0 || y != 0 || inp != 0 || e != 0) {

						ClosedLoop closedLoopX = ClosedLoop.builder().machineRunId(machineRun)
								.machineDataLoopTimestamp(
										timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
												rMachineDataSet.getIoData().getTimestamp2()))
								.type(type)

								.hmi(hmi).proc(proc).dem(dem).y(y).inp(inp).e(e).build();
						closedLoops.add(closedLoopX);
					}
				}
			}

			closedLoops.forEach(item -> {
				try {
					tempClosedloops.addRow(item.getMachineDataLoopTimestamp(), item.getDem(), item.getE(),
							item.getHmi(), item.getInp(), item.getProc(), item.getY(), item.getMachineRunId().getID(),
							item.getType().getId());
				} catch (SQLServerException e1) {
					e1.printStackTrace();
				}
			});

			return tempClosedloops;
		}


		private void setMachineRunPsConfig() {
			
				PSConfiguration psc =  PSConfiguration.builder()
						.acidDensity(rMachineDataSet.getProtectiveSystemData().getBalancingData().getAcidDensity())
						.baseDensity(rMachineDataSet.getProtectiveSystemData().getBalancingData().getBaseDensity())
						.machineRun(machineRun).build();
				psConfigRepo.save(psc);
			
		}


		private void setMachineRunStart() {
			if (machineRun.getStart() == null) {
				machineRun.setStart(timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
						rMachineDataSet.getIoData().getTimestamp2()));
				machineRun = machineRunRepo.save(machineRun);
			}
		}


		private int getStateValue(int mainState) {

			int stateValue = 0;
			switch (mainState) {
			case 0:
				stateValue = rMachineDataSet.getProcessControlData().getStartupState();
				break;
			case 1:
				stateValue = rMachineDataSet.getProcessControlData().getIdleState();
				break;
			case 2:
				stateValue = rMachineDataSet.getProcessControlData().getServiceState();
				break;
			case 3:
				stateValue = rMachineDataSet.getProcessControlData().getPreparationState();
				break;
			case 4:
				stateValue = rMachineDataSet.getProcessControlData().getTreatmentState();
				break;
			case 5:
				stateValue = rMachineDataSet.getProcessControlData().getPostprocessState();
				break;
			case 6:
				stateValue = rMachineDataSet.getProcessControlData().getDesinfectionState();
				break;
			default:
				break;
			}

			return stateValue;
		}


		private boolean matchFileVersion(String fileName) {

			String version = fileName.substring(38);
			String regex1 = "([3]{1}|[4-9]{1}|[0-9]{2,})\\.([7-9]{1}|[0-9]{2,})\\.\\d+_.*";
			String regex2 = "3\\.6\\.\\d+_t9([0-9][4-9]|[1-9][0-9]).*";

			if (version.matches(regex1)) {
				return true;
			}
			if (version.matches(regex2)) {
				return true;
			}

			return false;
		}
		
		private boolean matchVersion(String fileName) {

			String[] a = fileName.split("_");
			String b = a[7];
			String version = b.substring(7);
			String regex = "([3][.]([9]|[0-9]{2,})[.]\\d+?)|([4-9][.]\\d+?[.]\\d+?)";
			
			log.info(version);

			return version.matches(regex);
		}
	}

}
