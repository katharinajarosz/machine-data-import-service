package com.hw.importservice.converter;

import java.sql.Timestamp;
import java.time.Instant;

/**
 * Converts a LabView time object into {@link Timestamp}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
public class TimestampConverter  {
	
	private static final long LAB_VIEW_UTC = -2082844800;
		
		/**
		 * @param seconds
		 * @param nano
		 * @return java sql timestamp
		 */
		public Timestamp convert(Long seconds, Long nano) {
			
			Instant instant = Instant.ofEpochSecond(seconds, nano);
			int nanos = instant.getNano();
			long unixUTC = LAB_VIEW_UTC + seconds;
			
			Instant i = Instant.ofEpochSecond(unixUTC);
			
			Timestamp utc = Timestamp.from(i);
			utc.setNanos(nanos);
			return utc;
		}

	}