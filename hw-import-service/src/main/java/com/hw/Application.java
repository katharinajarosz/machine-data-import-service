package com.hw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * The Class com.hw.Application.
 * 
 * Starts the application.
 */
@SpringBootApplication
public class Application {
			   
    	/**
	     * The main method.
	     *
	     * @param args the arguments
		 */
	    public static void main(String[] args) {
	        SpringApplication.run(Application.class, args); 	      
	       
	    }

}